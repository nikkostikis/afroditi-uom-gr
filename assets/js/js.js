function checkCompatibility(){
    if (window.DeviceMotionEvent) {
	     //alert("Motion is supported inside checkCompatibility");
        document.getElementById("yes").style.display="block";
	      document.getElementById("no").style.display="none";
    }
}
var accelabel = "Gyro";
function checkAndStart(t){
    if (window.DeviceMotionEvent) {
		    //alert("Motion is supported inside checkAndStart. t = " + t);
        //document.body.style.backgroundColor="#0FF";
          window.ondevicemotion = function(event) {
          //var axTemp = event.accelerationIncludingGravity.x*1;
          var axTemp = event.acceleration.x*1;
          if (axTemp == 0) {
            accelabel = "noGyro";
          }
          //var axTemp = event.accelerationIncludingGravity.x*1;
          //paxTemp = axTemp.toString();
          //document.getElementById('nameDiv').innerHTML = paxTemp;
        }
        setTimeout( function(){ AccOr(t) }, 3000 );
    }
    else {
        document.getElementById("no").style.display="block";
	      document.getElementById("yes").style.display="none";
    }
}

// Acceleration
var ax = 0;     var ay = 0;    var az = 0;
var axAr = new Array();    var ayAr = new Array();    var azAr = new Array();
var atime = new Array();
// Orientation
var oa = 0;    var ob = 0;    var og = 0;
var oaAr = new Array();    var obAr = new Array();    var ogAr = new Array();
var otime = new Array();
//Angular Velocity aka Rotation Rate
var ra = 0;    var rb = 0;    var rg = 0;
var raAr = new Array();    var rbAr = new Array();    var rgAr = new Array();
var rtime = new Array();

var ai = 0;  //accel array size
var oi = 0;  //orient array size
var ri = 0;  //rotation array size
var pax = " ";var pay = " ";var paz = " ";var pai = " ";var poa = " ";var pob = " ";var pog = " ";var poi = " ";var pra = " ";var prb = " ";var prg = " ";var pri = " ";var patime = " ";var potime = " ";var prtime = " ";
var intrval = 0;
function AccOr(t)
{
    setTimeout(function(){ fSubmit() }, t);
    var sec = 0;
    document.body.style.backgroundColor="#0F0";
    window.ondevicemotion = function(event) {
        if (accelabel == "Gyro") {
          ax = event.acceleration.x;
          ay = event.acceleration.y;
          az = event.acceleration.z;
        }
        else {
          ax = event.accelerationIncludingGravity.x;
          ay = event.accelerationIncludingGravity.y;
          az = event.accelerationIncludingGravity.z;

        }

        intrval = event.interval;

        ra = event.rotationRate.alpha;
        rb = event.rotationRate.beta;
        rg = event.rotationRate.gamma;

        ai = ai + 1;
        axAr[ai] = ax;
        ayAr[ai] = ay;
        azAr[ai] = az;

        ri = ri + 1;
        raAr[ri] = ra;
        rbAr[ri] = rb;
        rgAr[ri] = rg;

        var d = new Date();
        h = d.getHours();
        hh = h.toString();
        if (h<10) { hh="0" + hh;}
        m = d.getMinutes();
        mm = m.toString();
        if (m<10) { mm="0" + mm;}
        s = d.getSeconds();
        ss = s.toString();
        if (s<10) { ss="0" + ss;}
        ms = d.getMilliseconds();
        mms = ms.toString();
        if (ms<10) { mms="00" + mms;}
        else {if (ms<100) { mms="0" + mms;}}

        atime[ai] = hh+mm+ss+mms;

        rtime[ri] = hh+mm+ss+mms;
    };
    window.ondeviceorientation = function(event) {
        oa = event.alpha;
        ob = event.beta;
        og = event.gamma;

        oi = oi + 1;
        oaAr[oi] = oa;
        obAr[oi] = ob;
        ogAr[oi] = og;

        var od = new Date();
        oh = od.getHours();
        ohh = oh.toString();
        if (oh<10) { ohh="0" + ohh;}
        om = od.getMinutes();
        omm = om.toString();
        if (om<10) { omm="0" + omm;}
        os = od.getSeconds();
        oss = os.toString();
        if (os<10) { oss="0" + oss;}
        oms = od.getMilliseconds();
        omms = oms.toString();
        if (oms<10) { omms="00" + omms;}
        else {if (oms<100) { omms="0" + omms;}}

        otime[oi] = ohh+omm+oss+omms;

    };
    setInterval(function() {
        var seclbl = document.getElementById("seclbl");
        sec=sec+1;
        seclbl.innerHTML= "<h2>" + sec + "</h2>";
    }, 1000);
    setInterval(function() {
        var tags = document.getElementById("tags");
        tags.innerHTML= "Sample no: " + ai +
        "<br><br>Time: " + hh + ":" + mm + ":" + ss +
        "<br><br>Acceleration in m/s^2 <br>" + "x: " + ax.toPrecision(3) + " / y: " + ay.toPrecision(3) + " / z: " + az.toPrecision(3) +
        "<br><br>Orientation in degrees <br>" + "a: " + ra.toPrecision(3) + " / b: " + rb.toPrecision(3) + " / g: " + rg.toPrecision(3) +
        "<br><br>sampling rate: " + Math.round(1/intrval) + "Hz";
    }, 500);
}
function setValues()
{
    pax = axAr.toString();
    pay = ayAr.toString();
    paz = azAr.toString();
    pai = ai.toString();
    patime = atime.toString();

    poa = oaAr.toString();
    pob = obAr.toString();
    pog = ogAr.toString();
    poi = oi.toString();
    potime = otime.toString();

    pra = raAr.toString();
    prb = rbAr.toString();
    prg = rgAr.toString();
    pri = ri.toString();

    document.dataForm.pax.value=pax;
    document.dataForm.pay.value=pay;
    document.dataForm.paz.value=paz;
    document.dataForm.pai.value=pai;
    document.dataForm.patime.value=patime;

    document.dataForm.poa.value=poa;
    document.dataForm.pob.value=pob;
    document.dataForm.pog.value=pog;
    document.dataForm.poi.value=poi;
    document.dataForm.potime.value=potime;

    document.dataForm.pra.value=pra;
    document.dataForm.prb.value=prb;
    document.dataForm.prg.value=prg;
    document.dataForm.pri.value=pri;
    document.dataForm.prtime.value=prtime;

}
function fSubmit(){
    setValues();
    document.body.style.backgroundColor="#FF0";
    document.dataForm.submit();
}
function showNext(idArray){
  for (i=0; i<idArray.length; i++) {
    console.log(idArray[i]);
    d=document.getElementById(idArray[i]);
    d.style.display="block";
  }
}
function loadSession(sT,sHz,sDisp,sD){
	ddT=document.getElementById("name");
	ddHz=document.getElementById("hertz");
	ddDisp=document.getElementById("disp");
  ddD=document.getElementById("duration");
	ddStart=document.getElementById("start");

	ddT.style.display="block";
	ddHz.style.display="block";
	ddDisp.style.display="block";
  ddD.style.display="block";
	ddStart.style.display="block";

	dT=document.getElementById("nameText");
	dHz=document.getElementById("hertzText");
	dDisp=document.getElementById("dispText");
  dD=document.getElementById("durationText");

	dT.value=sT;
	dHz.value=sHz;
	dDisp.value=sDisp;
  dD.value=sD;
}

$(function() {
    $( ".noteButton" ).click(function() {
      //$( "#note" ).toggle();
      $( "#note" ).slideToggle("fast");
      //$( "#note" ).animate({
      //  opacity: "toggle"
      //}, "slow" );
    });
});
