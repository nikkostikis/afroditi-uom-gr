<?php session_start();?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width,user-scalable=no" />
<meta charset="UTF-8"/>
<title>iPhone TremorSense</title>
<link rel="stylesheet" type="text/css" href="assets/css/custom.css">
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="assets/js/js.js"></script>
</head>
<body>

<div id="content">
    <h3>iPhone TremorSense - Start Page</h3>
    <a class="noteButton" href="#">(info)</a>
    <div id="note"><p>This is a web app used for research purposes.</br>
      It constantly undergoes changes.</br>
      If you choose to use it and want/need any kind of feedback,</br>
      or want to retrieve the data it submits, contact Nick Kostikis
      at nikkostikis using the gmail domain.</br></br>
      <a class="noteButton" href="#">(close)</a>
    </p></div>
    <div id="yes">
    <?php
        if (isset($_SESSION['sType'])) {
                $sT=$_SESSION['sType'];
                $sHz=$_SESSION['sHertz'];
                $sDisp=$_SESSION['sDisp'];
                $sD=$_SESSION['sD'];
                echo "<button type='button' name='sameSession' id='sameSession' onClick='loadSession(\"$sT\",\"$sHz\",\"$sDisp\",\"$sD\");'>Same Session</button>";
        }
    ?>
    <form name="info" method="post" action="main.php"><p>
	      <div id="name">Phone type:<input type="text" id="nameText" name="nameText" onChange="showNext(['hertz'])"/></div><br/>
        <div id="hertz">Hz:<input type="text" id="hertzText" name="hertzText" onChange="showNext(['disp'])"/></div><br/>
        <div id="disp">Cm:<input type="text" id="dispText" name="dispText" onChange="showNext(['duration', 'start'])"/></div><br/>
        <div id="duration">Recording duration in thousands of milliSeconds (default: 10000):
        <input type="text" id="durationText" name="durationText" value="10000" />
        </div>
        <!--<input type="reset" value="Reset"/>-->
        </p>
    </form>
        <div id="reset"><p><a href="killSession.php"><h3>Reset the form</h3></a></p></div>
        <div id="start"><p><a href="javascript:void(0)" onclick="document.info.submit();"><h3>Start recording</h3></a></p></div>

    </div>
    <div id="no">
    Your browser does not support Device Orientation and Motion API.
    </div>
</div>
<script>
    checkCompatibility();
</script>
</body>
</html>
